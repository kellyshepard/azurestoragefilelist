﻿using System;
using System.Collections.Generic;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace WebApplication1
{
    public partial class index : System.Web.UI.Page
    {
        string CDNURL = "https://hworgdev.blob.core.windows.net";

        protected void Page_Load(object sender, EventArgs e)
        {
            string containerNameGuides = "hrcclientdocs";
            string dirGuides = "knowledgebase/";
            //string CDNURL = ConfigurationManager.AppSettings["CDNURL"];
            

            if (!string.IsNullOrEmpty(containerNameGuides))
            {
                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("StorageConnectionString"));

                // Create the blob client. 
                CloudBlobClient blobClientGuides = storageAccount.CreateCloudBlobClient();

                // Check for valid container
                // Retrieve reference to a previously created container.
                try
                {
                    CloudBlobContainer containerGuides = blobClientGuides.GetContainerReference(containerNameGuides);

                    if (containerGuides.Exists(null, null))
                    {
                        var results = containerGuides.ListBlobs(dirGuides, false, BlobListingDetails.None, null, null);
                        ListBlobsGuides(results);
                        //ListBlobsGuides(containerGuides, dirGuides, CDNURL);

                    }
                    else
                    {
                        // Label1.Text = "Invalid container";
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    // Label1.Text = "Unknown Error";
                }
            }
            else
            {
                //  Label1.Text = "Please provide container name";
            }
        }


        private void ListBlobsGuides(IEnumerable<IListBlobItem> results)
        {
            //var results = containerGuides.ListBlobs(dirGuides, false, BlobListingDetails.None, null, null);

            //foreach (var result in results)
            //{
            //    if (result.GetType().Equals(typeof(CloudBlobDirectory)))
            //    {
            //    }
            //}
            //var blobs = results
            //            .Cast<CloudBlockBlob>() 
            //            .OrderByDescending(b => b.Properties.LastModified);
           
            //foreach (var blob in blobs)
            foreach (var result in results)
            {
                if (result.GetType() == typeof(CloudBlockBlob))
                {
                    //{
                    //CloudBlockBlob blobGuides = (CloudBlockBlob)itemGuides;
                    CloudBlockBlob blob = (CloudBlockBlob)result;
                    //   Label1.Text = blob.Uri.AbsolutePath;

                    lblGuides.Text += "<p><a style='display: inline;'target=_blank href=\"" + CDNURL + blob.Uri.AbsolutePath + "\">" + blob.Name.Substring(blob.Name.LastIndexOf("/") + 1) + "</a>&nbsp;&nbsp;<span>Last Modified: " + string.Format("{0:MM/dd/yy h:mm:ss tt}", blob.Properties.LastModified) + " </span></p>";
                }
                else
                {
                    CloudBlobDirectory directory = (CloudBlobDirectory)result;
                    pnldirectory.Visible = true;
                    // Uncomment this line to list the link
                    lblDirectory.Text += directory.Prefix;

                    // Uncomment these two lines to list all files out underneath a directory
                    var blobs = directory.ListBlobs();
                    ListBlobsGuides(blobs);
                }
                pnlGuides.Visible = true;
            }
            return;
        }
    }
}




