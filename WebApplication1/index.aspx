﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="WebApplication1.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" >
   <title>Document Browser</title>
   <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript" ></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(".collapsed").click(function (e) {

    this.setAttribute('class', 'collapse');
    var x = this.getAttribute('aria-expanded');
    var y = "";
    if (x == "true") {
        x = "false"
        y = "true"
    } else {
        x = "true"
        y = "false"
    }
    this.setAttribute('aria-expanded', x)
    this.setAttribute('aria-selected', x)

    $(this).next().attr('aria-hidden', function (index, attr) {
        return attr == x ? y : x;
    });
});
    </script>
    <style>
        a {
    color: #0D8484;
    text-decoration: none !important;
    display: block;
    outline: none;
    font-weight: 500;
}
        .collapsed {
  display: block; }

.icon {
  display: inline-block; }

.collapse-arrow .icon {
  transform: rotate(-180deg);
  transition: .3s transform ease;
  display: inline-block;
  outline: rgb(66, 66, 66) dotted 0px;
  outline-offset: 0px; }

.collapse-arrow .collapsed .icon {
  transform: rotate(0deg); }

.collapse show{
  
  }
.collapse:not(.show) {
    display: none;
}



    </style>
</head>
<body>
     <asp:Panel ID="pnlGuides" runat="server" Visible="true">
<div>
<h3 class="productH3">Guides</h3>

    
            <div class="collapse-arrow">
<h6><img src="horizontal_folder.png " style="display: inline;" /><a data-toggle="collapse" class="collapsed" href="#collapseFilterGeneral" tabindex="0" aria-controls="collapseFilterGeneral" aria-expanded="false" aria-selected="false" style="display: inline;">General&nbsp;&nbsp;
          <span class="icon">
             <svg version="1.1" id="Capa_1General" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         width="12px" height="12px" viewBox="0 0 451.847 451.847" style="enable-background:new 0 0 451.847 451.847;"
         xml:space="preserve">
      <g>
        <path d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751
          c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0
          c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"/>
      </g>
             </svg>
          </span>
         
        </a></h6>
  <div class="collapse" id="collapseFilterGeneral" aria-live="polite" aria-labelledby="collapseFilterGeneral" aria-hidden="true">
          <div class="detailsBody" tabindex="0"> <asp:Label id="lblGuides" runat="server" Text="" CssClass="downloadsDataList" ></asp:Label><asp:Label id="Label1" runat="server" Text="" CssClass="downloadsDataList" ></asp:Label>

              <asp:Panel ID="pnldirectory" runat="server" Visible="false">
                              <div class="collapse-arrow">
<h6><img src="horizontal_folder.png " style="display: inline;" /><a data-toggle="collapse" class="collapsed" href="#collapseFilterDirectory" tabindex="0" aria-controls="collapseFilterDirectory" aria-expanded="false" aria-selected="false" style="display: inline;"> <asp:Label id="lblDirectory" runat="server" Text="" CssClass="downloadsDataList" ></asp:Label>&nbsp;&nbsp;
          <span class="icon">
             <svg version="1.1" id="Capa_1Directory" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         width="12px" height="12px" viewBox="0 0 451.847 451.847" style="enable-background:new 0 0 451.847 451.847;"
         xml:space="preserve">
      <g>
        <path d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751
          c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0
          c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z"/>
      </g>
             </svg>
          </span>
         
        </a></h6>
  <div class="collapse" id="collapseFilterDirectory" aria-live="polite" aria-labelledby="collapseFilterGeneral" aria-hidden="true">
          <div class="detailsBody" tabindex="0">
              <asp:Label id="lblDirectoryContents" runat="server" Text="" CssClass="downloadsDataList" ></asp:Label>
               </div></div></div>
       



              </asp:Panel>

              </div></div></div>
       


 

     </div>
    </asp:Panel>
    
</body>
</html>
