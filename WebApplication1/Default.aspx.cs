using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

public partial class _Default : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string containerName = Request.QueryString["container"];
        string dir = Request.QueryString["dir"];
        //string CDNURL = ConfigurationManager.AppSettings["CDNURL"];
        string CDNURL = "http://cdn-clientdocs.healthwise.org";

        

        if (!string.IsNullOrEmpty(containerName)) {
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            CloudConfigurationManager.GetSetting("StorageConnectionString"));

            // Create the blob client. 
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
         
            // Check for valid container
            // Retrieve reference to a previously created container.
            try
            {
                CloudBlobContainer container = blobClient.GetContainerReference(containerName);

                if (container.Exists(null,null)) {
                    listBlobs(container,dir,CDNURL);
                   
                }
                else
                {
                    Label1.Text = "Invalid container";
                }
            }
            catch
            {
                Label1.Text = "Unknown Error";
            }
        }
        else
        {
            Label1.Text = "Please provide container name";
        }
    }

    protected string parseString(string _strValue, string dir)
    {
        string toremove = dir;
        string strReturn = _strValue.Replace(toremove, "");

        return (strReturn);
    }


    private void listBlobs(CloudBlobContainer container, string dir, string CDNURL)
    {
        // Loop over items within the container and output the length and URI.
        string host = HttpContext.Current.Request.Url.Host;
       
        foreach (IListBlobItem item in container.ListBlobs(dir, false, BlobListingDetails.None, null, null))
        {
            if (item.GetType() == typeof(CloudBlockBlob))
            {
                CloudBlockBlob blob = (CloudBlockBlob)item;
               
                string filetype = "file.png";

                if (blob.Properties.ContentType == "application/x-zip-compressed")
                {
                    filetype = "zip.png";
                }
                if (blob.Properties.ContentType == "application/octet-stream")
                {
                    filetype = "app.png";
                }
                if (blob.Properties.ContentType == "application/pdf")
                {
                    filetype = "pdf.png";
                }
                if (blob.Properties.ContentType == "text/xml")
                {
                    filetype = "text.png";
                }
                if (blob.Properties.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    filetype = "excel.png";
                }
                if (string.IsNullOrEmpty(CDNURL))
                {

                    Label1.Text += "<tr><td><img src=" + filetype + "></td><td> <a target=_blank href=\"" + CDNURL + blob.Uri.AbsoluteUri + "\">" + blob.Name.Substring(blob.Name.LastIndexOf("/") + 1) + "</a> <br /></td></tr>";
                }
                else
                {
                    //Response.Write(blob.Uri.AbsolutePath);
                    
                    if (host == "hrcstorage.azurewebsites.net")
                    {
                        string newURL = "https://hworgclientservices.blob.core.windows.net";
                       // Label1.Text += "<tr><td><img src=" + filetype + "></td><td> <a target=_blank href=\""  + blob.Uri.AbsolutePath + "\">" + blob.Name.Substring(blob.Name.LastIndexOf("/") + 1) + "</a> <br /></td></tr>";
                        Label1.Text += "<tr><td><img src=" + filetype + "></td><td> <a target=_blank href=\"" + newURL + blob.Uri.AbsolutePath + "\">" + blob.Name.Substring(blob.Name.LastIndexOf("/") + 1) + "</a> - Last Modified: " + String.Format("{0:MM/dd/yy h:mm:ss tt}", blob.Properties.LastModified) + "</td></tr>";
                    }
                    else
                    {
 Label1.Text += "<tr><td><img src=" + filetype + "></td><td> <a target=_blank href=\"" + CDNURL + blob.Uri.AbsolutePath + "\">" + blob.Name.Substring(blob.Name.LastIndexOf("/") + 1) + "</a> - Last Modified: " + String.Format("{0:MM/dd/yy h:mm:ss tt}", blob.Properties.LastModified) + " </td></tr>";

                    }
                }
            }
            else if(item.GetType() == typeof(CloudBlobDirectory))
            {
                CloudBlobDirectory directory = (CloudBlobDirectory)item;
                Label1.Text += "<tr><td><img src=folder.png></td><td> <a href=\"?container=" + container.Name + "&dir=" + directory.Prefix + "&inside=1\">" + parseString(directory.Prefix, dir) + "</a><br /></td></tr>";
            }
        }
    }

  


}